package com.geenbedrijf.webshop.inventory.controller;

import com.geenbedrijf.webshop.inventory.model.Article;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@CrossOrigin(maxAge = 3600)
public class InventoryController {

    @GetMapping("/inventory")
    public List<Article> getInventory(String search, String content) {
        log.info("GET INVENTORY");
        ArrayList<Article> list = new ArrayList<>();
        list.add(new Article("1", "Laptop Toshiba", "A normal laptop Toshiba", 1235.2, 14));
        list.add(new Article("2", "Laptop HP", "A normal laptop HP", 1215.2,6));
        return list;
    }
}
