package com.geenbedrijf.webshop.inventory.model;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class Article {

    @NonNull
    private String id;

    @NonNull
    private String name;

    @NonNull
    private String description;

    @NonNull
    private double price;

    private int number;


}
