import React from "react";
import "./App.css";
import HeaderContent from "./components/headerContent";
import BodyContent from "./components/bodyContent";

function App() {
  return (
    <React.Fragment>
      <HeaderContent />
      <BodyContent />
    </React.Fragment>
  );
}

export default App;
