import React, { Component } from "react";
class Article extends Component {
  state = {
    articles: [],
  };
  render() {
    return (
      <div className="container">
        <h1>My Articles</h1>
        <div className="col-xs-12 list-group">
          {this.state.articles.map((article) => (
            <div
              className="list-group-item list-group-item-action flex-column align-items-start"
              key={article.id}
            >
              <div className="d-flex w-100 justify-content-between">
                <h5 className="mb-1">{article.name}</h5>
                <span className="badge badge-primary badge-pill">
                  {article.number}
                </span>
              </div>
              <p className="mb-1">{article.description}</p>
              <p className="mb-1">€{article.price}</p>
              <small>Donec id elit non mi porta.</small>
            </div>
          ))}
        </div>
      </div>
    );
  }

  componentDidMount() {
    fetch("http://localhost:8080/inventory")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ articles: data });
      })
      .catch(console.log);
  }
}

export default Article;
