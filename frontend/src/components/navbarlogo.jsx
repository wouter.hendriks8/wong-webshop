import React, { Component } from "react";
import logo from "./../images/logo.jpg";
import WelcomeText from "./welcome";

class NavBar extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
          <a className="navbar-brand" href="#">
            <img src={logo} alt="logo" style={{ width: "40px" }} />
          </a>
          <WelcomeText />
        </nav>
      </React.Fragment>
    );
  }
}

export default NavBar;
