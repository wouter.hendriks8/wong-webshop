import React, { Component } from "react";
import NavBarLogo from "./navbarlogo";
import NavBar from "./navbar";
class HeaderContent extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <NavBarLogo />
        <NavBar />
      </React.Fragment>
    );
  }
}

export default HeaderContent;
